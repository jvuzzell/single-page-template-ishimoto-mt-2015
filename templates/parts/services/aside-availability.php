<aside class="show-for-large-up large-offset-1 large-3 columns">
	<div id="availabilty"> 
		<div style="border: 5px solid #ffffff; margin-bottom:20px;">
			<table class="pricing-table">
				<tr><th colspan="2" class="header">Availability</th></tr>
				<tr><td><strong>Sunday</strong></td><td>11am - 5pm</td></tr>
				<tr><td><strong>Monday</strong></td><td>11am - 5pm</td></tr>
				<tr><td><strong>Tuesday</strong></td><td>Closed</td></tr>
				<tr><td><strong>Wednesday</strong></td><td>11am - 5pm</td></tr>
				<tr><td><strong>Thursday</strong></td><td>11am - 5pm</td></tr>
				<tr><td><strong>Friday</strong></td><td>11am - 5pm</td></tr>
				<tr><td><strong>Saturday</strong></td><td>11am - 5pm</td></tr>
			</table>
		</div>
		<div><a href="#"><button style="width:100%;">Book Online</button></a>
		</div>
		<div id="ask">
			Call for an appointment <br/> 555-555-5555 
		</div>
	</div>
</aside>
			
