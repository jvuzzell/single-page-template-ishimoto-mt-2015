<div class="small-9 medium-10 large-7 large-offset-1 small-centered large-uncentered columns " id="services-wrap"> 
	<article class="service row">
		<div class="small-12 medium-3 columns">
			<img class="scene" src="<?php print_r($paths['images'].'scene1.png');?>">
		</div>
		<div class="small-12 medium-9 columns description">
			<h3 class="name">Swedish Massage</h3>
			<div class="show-for-medium-up">
				<h5 class="rate">$65/hour</h5>
				<p class="excerpt">
				Swedish Massage is a very relaxing and therapeutic style of bodywork. It combines oils or lotion with an array of strokes such as rolling, kneading, and percussion to help the body improve its circulation.</p>
			</div>
		</div>
	</article>
	
	
	<article class="service row">
		<div class="small-12 medium-3 columns">
			<img class="scene" src="<?php print_r($paths['images'].'scene2.png');?>">
		</div>
		<div class="small-12 medium-9 columns description">
			<h3 class="name">Deep Tissue Massage</h3>
			<div class="show-for-medium-up">
				<h5 class="rate">$75/hour</h5>
				<p class="excerpt">
				Deep Tissue Massage is a highly effective method for releasing chronic stress areas due to misalignment, repetitive motions, and past injuries.</p>
			</div>
		</div>
	</article>
	
	
	<article class="service row">
		<div class="small-12 medium-3 columns">
			<img class="scene" src="<?php print_r($paths['images'].'scene3.png');?>">
		</div>
		<div class="small-12 medium-9 columns description">
			<h3 class="name">Hot Stone Therapy</h3>
			<div class="show-for-medium-up">
				<h5 class="rate">$75/hour</h5>
				<p class="excerpt">
				Hot Stone Therapy is a style of massage where heated stones are placed at specific sites on your body to deepen relaxation and promote circulation in your muscles. 
				</p>
			</div>
		</div>
	</article>
	
	<article class="service row">
		<div class="small-12 medium-3 columns">
			<img class="scene" src="<?php print_r($paths['images'].'scene3.png');?>">
		</div>
		<div class="small-12 medium-9 columns description">
			<h3 class="name">Chair Massage</h3>
			<div class="show-for-medium-up">
				<h5 class="rate">$30/30min</h5>
				<p class="excerpt">
				Chair massage is performed in a specialized ergonomic chair. In addition to the usual health benefits of massage, another benefit of chair massage is that it is done over the clothing.	
				</p>
			</div>
		</div>
	</article>
</div>