<div class="small-12 columns small-centered medium-uncentered" id="embed-map" style="padding:0;">
	<iframe class="show-for-medium-up"src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3100.8356206839817!2d-77.02611189999999!3d38.99624779999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7c8ba4021573b%3A0xdd265a146e65dbc9!2s916+Ellsworth+Dr%2C+Silver+Spring%2C+MD+20910!5e0!3m2!1sen!2sus!4v1426128765831" width="100%" height="350" frameborder="0" style="border:0; margin:0 auto; pointer-events:none;"></iframe>
	
	<iframe class="show-for-small-only" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3100.8356206839817!2d-77.02611189999999!3d38.99624779999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7c8ba4021573b%3A0xdd265a146e65dbc9!2s916+Ellsworth+Dr%2C+Silver+Spring%2C+MD+20910!5e0!3m2!1sen!2sus!4v1426128765831" width="100%" height="260" frameborder="0" style="border:0; margin:0 auto; pointer-events:none;"></iframe>

</div>