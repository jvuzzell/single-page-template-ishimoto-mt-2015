<!-- about -->
<section class="show-for-large-up large-12 row" id="about-janet-wrap">
	<div id="about">
		<div class="large-7 large-offset-1 columns" id="info-wrap">
			<div id="info">
				<h3>About Janet Ishimoto</h3>
				<h5>Proprietor & Practicioner</h5>
				<p>I am a graduate of the Potomac Massage Training Institute in Washington, D.C. I am nationally certified in therapeutic massage by the National Certification Board for Therapeutic Massage and Bodywork, a member of the American Massage Therapy Association, and a Licensed Massage Therapist in the State of Maryland. I believe that massage therapy is beneficial to the mind and spirit. It also provides benefits in enabling healing from physical injuries and stresses. I work from my home, where I live with my husband, son, and three cats.</p>
			</div>
			<div id="portrait">
				<img src="<?php print_r($paths['images'].'banner-image4.png');?>"/>
			</div>
		</div>

		<aside class="large-3 large-offset-1 columns"></aside>
	</div>
</section>
<!-- end about -->