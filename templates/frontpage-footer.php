<!-- footer -->
<footer>
	<div id="foot-wrap">
		<section class="row" id="ask">
			<div id="cta-foot-button">
				<a href="#"><button>Book Online</button></a>
			</div>
			<img class="stones-candle-img" src="<?php print_r($paths['images'].'stones-w-candle2.png');?>"/>
			<div style="text-align:center;">
				Or call for an appointment: 555-555-5555 
			</div>	
			
		</section>
	</div>
	<section class="row" id="signature" style="text-align:center;"> 
		Copyright 2015. Janet Ishimoto. Site Developed by <a href="http://www.joshuauzzell.com">Joshua Uzzell.</a>
	</section>
</footer>
<!-- footer -->