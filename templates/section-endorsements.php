<!-- endorsements -->
<div id="testimonials-wrap">
	<section class="show-for-medium-up row">
		<div id="testimonials" class="medium-10 columns medium-centered">
			<h3>Testimonial</h3>
			<p><strong style="font-family:serif;">"</strong>Janet is one of the most skilled practitioners I've ever had... Her technique is precise and wonderful; she always finds knots I didn't even know I had! I walk out of her sessions feeling great, with a sense of deep relaxation. I always look forward to my sessions with Janet, and would recommend her to anyone looking for an excellent, professional massage with tangible results.<strong style="font-family:serif;">"</strong></p>
			<h5>Kimberly W, Client of 1 Year</h5>
		</div>
	</section>
</div>
<!-- end endorsements -->