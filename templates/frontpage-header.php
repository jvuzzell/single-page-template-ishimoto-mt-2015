<!-- header -->
<nav class="show-for-medium-up row" id="header-nav">
	<img class="float right" src="<?php print_r($paths['images'].'partner-logo.png');?>" alt="American Massage Therapist Association" />
	<p class="float left" >Call for an appointment: 555-555-5555</span> 
</nav>

<div class="float left show-for-small-up hide-for-large-up" id="first-scenery"><img src="<?php print_r($paths['images'].'accent3.png');?>" alt="bamboo on stones"></div>

<!-- banner -->
<header> 
	<div class="float right show-for-medium-up" id="scenery"><img src="<?php print_r($paths['images'].'banner-image.png');?>" alt="bamboo on stones"></div>
	<section class="row small-centered large-uncentered" id="banner">
		
		<div id="header-logo"><img src="<?php print_r($paths['images'].'ishimoto-massage-logo.png');?>" alt="Ishimoto Massage Therapy"></div>
		<div class="">
			<ul>
				<li>Massage therapy is beneficial to the mind and spirit.</li>
				<li>Enables healing from physical injuries and stresses.</li>
				<li>Enhances immunity by stimulating lymph flow.</li>
			</ul>	
			<a class="show-for-medium-up" href="" id="cta-button1"><button>Book Online</button></a>
		</div>
	</section>
			
	<div class="show-for-medium-only" id="medium-scenery">
		<img src="<?php print_r($paths['images'].'medium-banner-image.png');?>"/> 
	</div>
	<div class="show-for-small-only" id="small-scenery" style="width:100%; overflow:hidden no-scroll;position:relative;">
		<img src="<?php print_r($paths['images'].'small-banner-image.png');?>"/> 
	</div>
	<section class="slogan" >
		<div class="row" id="slogan"><img src="<?php print_r($paths['images'].'ishimoto-massage-slogan.png');?>" alt="A solid foundation for self care"></div>
	</section>
</header>
<!-- banner -->

<nav class="show-for-small-only" style="height:200px;background:#f3f5e7;">
	<div style="width:152px; margin:25px auto 0 auto;">
			<a href="#" data-reveal-id="online-booking"><button>Book Online</button></a>
	</div>
	<div style=" margin-top:-25px;text-align:center;">
		Or call for an appointment: 555-555-5555 
	</div>	
</nav>
<!-- end header --> 