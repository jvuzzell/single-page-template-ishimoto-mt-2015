<!-- location (Google Map) -->
<div id="location-wrap">
	<section class="row" id="location" style="margin-left:0px;margin-right:0px;max-width:100%;">
		<?php include(TEMPLATES_PATH."parts/location/google-map.php"); //include Google Map?>

		<div class="show-for-small-only small-12 columns small-centered" id="info">
			<br/>
			<p class="show-for-medium-up">We are conveniently located in the heart of Downtown Silver Spring. Book your appointment during your lunch break or come through on the weekend. We are here for you.</p>
			<dir style="padding-left:0px; padding-bottom: 20px;font-size:1.25em; letter-spacing:0.035em; line-height:1.2em;"><em>916 Ellsworth Dr. <br/> Silver Spring, MD 20910</em></dir>
			<p></p>
		</div>
	
	</section>
</div>	
<!-- end location -->