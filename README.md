# ishimoto-mt

A mobile responsive one page website for Massage Therapist Janet Ishimoto. Just a template, no functionality.

## Synopsis
Graphic designer and Web Developer Joshua Uzzell built a mobile responsive one page website that's ready for Wordpress or other custom CMS integration. The site was built for client Janet Ishimoto. 

The project is very straightforward and only the defines the visual structure of the would be site. Functionality has not been developed for this template yet. 

## Tests

Live View: http://www.joshuauzzell.com/devprojects/ishimoto-mt

## API Reference

This site was built using HTML 5 and CSS and Zurb Foundation 5.5.1. 

Zurb Foundation 5 Docs - http://foundation.zurb.com/sites/docs/v/5.5.3/

## Contributors

Developer/ Designer: Joshua Uzzell http://www.joshuauzzell.com

## License

Copyright 2016, Joshua Uzzell. Not for public use or redistribution.
