<?php 
	$base_url = "http://".$_SERVER['SERVER_NAME']."/";
	
	$paths = array(
        "images"   => "http://". $_SERVER['SERVER_NAME'] . "/app/assets/img/",
        "templates" => "http://". $_SERVER['SERVER_NAME'] . "/templates/",
        "library"   => "http://". $_SERVER['SERVER_NAME'] . "/app/library/", 
        "src" 	    => "http://". $_SERVER['SERVER_NAME'] . "/app/src/"
    );
	
	define("LIBRARY_PATH", $_SERVER["DOCUMENT_ROOT"]."/app/library/");
     
	define("TEMPLATES_PATH", $_SERVER["DOCUMENT_ROOT"]."/templates/");
	
 	define("DIRECTORY_PATH", $_SERVER["DOCUMENT_ROOT"]."/");
 		
?>