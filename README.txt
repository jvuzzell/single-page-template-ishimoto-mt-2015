Project: Responsive Website Template - One Pager
Client: Janet Ishimoto 
Developer: Joshua Uzzell
Developer URL: http://www.joshuauzzell.com

Base Framework: Zurb Foundation 5.5.1
Base Framework URL: http://foundation.zurb.com/sites/docs/v/5.5.3/

Project Published Date: February 07, 2016
Readme Published: February 07, 2016

Synopsis:
Graphic designer and Web Developer Joshua Uzzell built a mobile responsive one page website that's ready for Wordpress or other custom CMS integration. The site was built for client Janet Ishimoto. 

The project is very straightforward and only the defines the visual structure of the would be site. Functionality has not been developed for this template yet. 

Copyright 2016, Joshua Uzzell. Not for public use or redistribution.
