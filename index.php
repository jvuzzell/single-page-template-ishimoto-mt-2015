<?php require("app/library/config.php");?>
<?php require("header.php"); ?>

<?php include(TEMPLATES_PATH."frontpage-header.php"); ?>
<main> 
	<?php include(TEMPLATES_PATH."section-services.php"); ?>
	<?php include(TEMPLATES_PATH."section-endorsements.php"); ?>
	<?php include(TEMPLATES_PATH."section-about.php"); ?>
	<?php include(TEMPLATES_PATH."section-location.php"); ?>		
</main>

<?php include(TEMPLATES_PATH."frontpage-footer.php"); ?>
<?php require("footer.php"); ?>